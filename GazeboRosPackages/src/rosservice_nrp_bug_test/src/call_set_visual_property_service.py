#!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
from gazebo_msgs.srv import SetVisualProperties
from random import randrange



def set_color_for_links(model_name="iiwa14",link_name="iiwa_link_1",visual_name="iiwa_link_1_visual",property_name="material:script:name",property_value="Gazebo/Red"):
    rospy.wait_for_service("/gazebo/set_visual_properties")
    try:
        add_color = rospy.ServiceProxy('/gazebo/set_visual_properties', SetVisualProperties)
        resp1 = add_color(model_name,link_name,visual_name,property_name,property_value)
        print(resp1)
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)
    


if __name__ == "__main__":
    visual_property_parameters={"model_name":"iiwa14","link_name":"iiwa_link_1","visual_name":"iiwa_link_1_visual","property_name":"material:script:name","property_value":"Gazebo/Red"}
    array=["Gazebo/Grey", "Gazebo/White", "Gazebo/Black", "Gazebo/Red", "Gazebo/Blue", "Gazebo/Yellow", "Gazebo/Purple", "Gazebo/Turquoise", "Gazebo/Orange", "Gazebo/Wood", "Gazebo/Gold"]
    for i in range(1,8):
        random_value = randrange(1,8)
        print("color for the link" + str(i) + " -->" + array[random_value])
        set_color_for_links(visual_property_parameters["model_name"],
                            "iiwa_link_"+str(i),
                            "iiwa_link_" + str(i) + "_visual",
                            visual_property_parameters["property_name"],
                            array[random_value],
                            )

#Executed once
import rospy
from gazebo_msgs.srv import SetVisualProperties


visual_property_parameters={"model_name":"iiwa14","link_name":"iiwa_link_1","visual_name":"iiwa_link_1_visual","property_name":"material:script:name","property_value":"Gazebo/Red"}
gazebo_color_material_list=["Gazebo/Grey","Gazebo/DarkGrey","Gazebo/White","Gazebo/FlatBlack","Gazebo/Black","Gazebo/Red","Gazebo/RedBright","Gazebo/Green","Gazebo/Blue","Gazebo/SkyBlue","Gazebo/Yellow","Gazebo/ZincYellow","Gazebo/DarkYellow","Gazebo/Purple","Gazebo/Turquoise","Gazebo/Orange","Gazebo/Indigo","Gazebo/WhiteGlow","Gazebo/RedGlow","Gazebo/GreenGlow","Gazebo/BlueGlow","Gazebo/YellowGlow","Gazebo/PurpleGlow","Gazebo/TurquoiseGlow","Gazebo/TurquoiseGlowOutline","Gazebo/RedTransparentOverlay","Gazebo/BlueTransparentOverlay","Gazebo/GreenTransparentOverlay","Gazebo/OrangeTransparentOverlay","Gazebo/DarkOrangeTransparentOverlay","Gazebo/RedTransparent","Gazebo/GreenTransparent","Gazebo/BlueTransparent","Gazebo/DarkMagentaTransparent","Gazebo/GreyTransparent","Gazebo/BlackTransparent","Gazebo/YellowTransparent","Gazebo/LightOn","Gazebo/LightOff","Gazebo/LightBlueLaser","Gazebo/BlueLaser","Gazebo/OrangeTransparent","Gazebo/JointAnchor","Gazebo/CoM","Gazebo/WoodFloor","Gazebo/CeilingTiled","Gazebo/PaintedWall","Gazebo/PioneerBody","Gazebo/Pioneer2Body","Gazebo/Gold","Gazebo/GreyGradientSky","Gazebo/CloudySky","Gazebo/WoodPallet","Gazebo/Wood","Gazebo/Bricks","Gazebo/Road","Gazebo/Residential","Gazebo/Tertiary","Gazebo/Pedestrian","Gazebo/Footway","Gazebo/Motorway","Gazebo/Lanes_6","Gazebo/Trunk","Gazebo/Lanes_4","Gazebo/Primary","Gazebo/Lanes_2","Gazebo/Secondary","Gazebo/Lane_1","Gazebo/Steps","Gazebo/GaussianCameraNoise","Gazebo/CameraDistortionMap","Gazebo/WideLensMap","Gazebo/CameraLensFlare","Gazebo/PointCloud","Gazebo/PointHandle","Gazebo/BuildingFrame","Gazebo/Runway","Gazebo/Grass","Gazebo/Editor","Gazebo/EditorPlane","Gazebo/DepthMap","Gazebo/XYZPoints","Gazebo/LaserScan1st","Gazebo/LaserScan2nd"]


rospy.wait_for_service("/gazebo/set_visual_properties")
add_color = rospy.ServiceProxy('/gazebo/set_visual_properties', SetVisualProperties)

@nrp.MapVariable("array", initial_value=gazebo_color_material_list)
@nrp.MapVariable("visual_property_parameters", initial_value=visual_property_parameters)
@nrp.MapVariable("add_color", initial_value=add_color)
@nrp.Robot2Neuron()
def Rosservice_call_transfer (t, array, visual_property_parameters, add_color):
    from random import randrange
    for i in range(1,8):
        status = False
        random_value = randrange(1,8)
        print("color for the link" + str(i) + " -->" + array.value[random_value])
        #while not (status):
        print("inside for loop ros node")
        status = add_color.value(visual_property_parameters.value["model_name"],
                        "iiwa_link_"+str(i),
                        "iiwa_link_" + str(i) + "_visual",
                        visual_property_parameters.value["property_name"],
                        array.value[random_value],
                        )
    

    
    


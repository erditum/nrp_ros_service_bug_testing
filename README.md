Models folder should be copied into $HBP/Models. Then run the script ./create-symlinks.sh inside $HBP/Model folders.
nrpstorage folder should be copied inside $HOME/.opt/nrpStorage.


# Testcase 1
## A bash script [model_set_visual_test.sh](model_set_visual_test.sh) is used to call rosservice /gazebo/set_visual_properties
For testing ros service /gazebo/set_visual_properties, run the script.
./model_set_visual_test.sh
# Observation
## The called ros-service works properly with given arguments in rapid succession.
![Bash script test](Test_results/GIF/Calling_with_bash_script/bash_script_call.gif "Bash script test")



# Testcase 2
## A [python script](GazeboRosPackages/src/rosservice_nrp_bug_test/src/call_set_visual_property_service.py ) is used to call rosservice /gazebo/set_visual_properties
`rosrun rosservice_nrp_bug_test call_set_visual_property_service.py`
# Observation
## Calling the rosservice with python code lead to skip for updating topics `/model/modify` and `/recording/model/modify` 
## Test 
![python test](Test_results/GIF/Calling_with_python/color_not_applied.gif "Python test")


## Fix 
![fixed situation](Test_results/GIF/Calling_with_python/color_applied.gif "Fixed situation")


# Testcase 3
## NRP Transfer-function [Rosservice_call_transfer.py](nrpStorage/nrp_rosservice_bug_testing_and_fixing/Rosservice_call_transfer.py) is used to call rosservice /gazebo/set_visual_properties

## Test 
![test situation](Test_results/GIF/Transfer_function/before_fix.gif "Test situation")
## Fix
![fix situation](Test_results/GIF/Transfer_function/after_fix.gif "Test situation")



To fix this issue, the following line is added into [gazebo_ros_api_plugin.cpp](Fixed_file/gazebo_ros_api_plugin.cpp)
```
 const sdf::ElementPtr material_script_name = link->GetSDF()

                                                    ->GetElement("visual")

                                                    ->GetElement("material")

                                                    ->GetElement("script")

                                                    ->GetElement("name");

  

  ros::Duration model_set_visual_property_timeout(10.0);

  ros::Time timeout = ros::Time::now() + model_set_visual_property_timeout;

  while (req.property_value != material_script_name->GetValue()->GetAsString())

  {

    if (ros::Time::now() > timeout){

      res.status_message = std::string("setVisualProperties: service timed out waiting for the requested changes to be applied to the simulation.");

      res.success = false;

      return true;

    }

  }

```

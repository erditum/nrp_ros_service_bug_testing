#!/bin/bash
echo "The arguments $1 $2 $3" 

#Colours
RED="\033[01;31m"
GREEN="\033[01;32m"
PURPLE="\033[01;35m"
BLUE="\033[01;34m"
YELLOW="\033[01;33m"
WHITE="\033[01;37m"
BLACK="\033[01;30m"
TURQUOISE="\033[00;36m"
NC="\033[00m"

first_color_set(){

    echo "Setting first colors"
    echo -e "Setting ${YELLOW}yellow${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_1', visual_name: 'iiwa_link_1_visual', property_name: 'material:script:name', property_value: 'Gazebo/Yellow'}" 
    echo -e "Setting ${YELLOW}Yellow${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_2', visual_name: 'iiwa_link_2_visual', property_name: 'material:script:name', property_value: 'Gazebo/Red'}" 
    echo -e "Setting ${RED}Red${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_3', visual_name: 'iiwa_link_3_visual', property_name: 'material:script:name', property_value: 'Gazebo/White'}" 
    echo -e "Setting ${WHITE}White${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_4', visual_name: 'iiwa_link_4_visual', property_name: 'material:script:name', property_value: 'Gazebo/Green'}" 
    echo -e "Setting ${GREEN}Green${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_5', visual_name: 'iiwa_link_5_visual', property_name: 'material:script:name', property_value: 'Gazebo/Black'}" 
    echo -e "Setting ${BLACK}Black${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_6', visual_name: 'iiwa_link_6_visual', property_name: 'material:script:name', property_value: 'Gazebo/Blue'}" 
    echo -e "Setting ${BLUE}Blue${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_7', visual_name: 'iiwa_link_7_visual', property_name: 'material:script:name', property_value: 'Gazebo/Turquoise'}" 
    echo -e "Setting ${TURQUOISE}Turquoise${NC} color to the first link"
}

second_color_set(){

    echo "Setting second colors"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_1', visual_name: 'iiwa_link_1_visual', property_name: 'material:script:name', property_value: 'Gazebo/Red'}" 
    echo -e "Setting ${RED}Red${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_2', visual_name: 'iiwa_link_2_visual', property_name: 'material:script:name', property_value: 'Gazebo/White'}" 
    echo -e "Setting ${WHITE}White${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_3', visual_name: 'iiwa_link_3_visual', property_name: 'material:script:name', property_value: 'Gazebo/Yellow'}" 
    echo -e "Setting ${YELLOW}Yellow${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_4', visual_name: 'iiwa_link_4_visual', property_name: 'material:script:name', property_value: 'Gazebo/Green'}" 
    echo -e "Setting ${GREEN}Green${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_5', visual_name: 'iiwa_link_5_visual', property_name: 'material:script:name', property_value: 'Gazebo/Black'}" 
    echo -e "Setting ${BLACK}Black${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_6', visual_name: 'iiwa_link_6_visual', property_name: 'material:script:name', property_value: 'Gazebo/Turquoise'}" 
    echo -e "Setting ${TURQUOISE}Turquoise${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_7', visual_name: 'iiwa_link_7_visual', property_name: 'material:script:name', property_value: 'Gazebo/Blue'}"
    echo -e "Setting ${BLUE}Blue${NC} color to the first link" 
}

third_color_set(){

    echo "Setting third colors"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_1', visual_name: 'iiwa_link_1_visual', property_name: 'material:script:name', property_value: 'Gazebo/White'}" 
    echo -e "Setting ${WHITE}White${NC} color to the first link"
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_2', visual_name: 'iiwa_link_2_visual', property_name: 'material:script:name', property_value: 'Gazebo/Red'}"
    echo -e "Setting ${RED}Red${NC} color to the first link" 
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_3', visual_name: 'iiwa_link_3_visual', property_name: 'material:script:name', property_value: 'Gazebo/Green'}"
    echo -e "Setting ${GREEN}Green${NC} color to the first link" 
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_4', visual_name: 'iiwa_link_4_visual', property_name: 'material:script:name', property_value: 'Gazebo/Yellow'}"
    echo -e "Setting ${YELLOW}Yellow${NC} color to the first link" 
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_5', visual_name: 'iiwa_link_5_visual', property_name: 'material:script:name', property_value: 'Gazebo/Blue'}"
    echo -e "Setting ${BLUE}Blue${NC} color to the first link" 
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_6', visual_name: 'iiwa_link_6_visual', property_name: 'material:script:name', property_value: 'Gazebo/Red'}"
    echo -e "Setting ${RED}Red${NC} color to the first link" 
    rosservice call /gazebo/set_visual_properties "{model_name: 'iiwa14', link_name: 'iiwa_link_7', visual_name: 'iiwa_link_7_visual', property_name: 'material:script:name', property_value: 'Gazebo/Black'}"
    echo -e "Setting ${BLACK}Black${NC} color to the first link"  

}

set_orginal_colors(){

    echo "Setting orginal colors"
   for i in {1..7}
    do
        rosservice call /gazebo/set_visual_properties "{model_name: iiwa14, link_name: iiwa_link_"$i", visual_name: iiwa_link_"$i"_visual, property_name: 'material:script:name', property_value: Gazebo/Orange}" 
    done
}



random_color_set(){
    echo "Setting random colors "
    array=("Gazebo/Grey" "Gazebo/White" "Gazebo/Black" "Gazebo/Red" "Gazebo/Blue" "Gazebo/Yellow" "Gazebo/Purple" "Gazebo/Turquoise" "Gazebo/Orange" "Gazebo/Wood" "Gazebo/Gold")
    for i in {1..7}
    do
        rand=$[$RANDOM % ${#array[@]}]
        echo "The selected for for link $i is ${array[$rand]}"
        rosservice call /gazebo/set_visual_properties "{model_name: iiwa14, link_name: iiwa_link_"$i", visual_name: iiwa_link_"$i"_visual, property_name: 'material:script:name', property_value: ${array[$rand]}}" 
    done
}

$1
